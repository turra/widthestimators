# WidthEstimators

[![pipeline status](https://gitlab.cern.ch/turra/widthestimators/badges/master/pipeline.svg)](https://gitlab.cern.ch/turra/widthestimators/badges/master/pipeline.svg)


This is a collection of functions to estimate the width (the scale) and the location of a distribution. It supports binned estimation (from ROOT histograms) and from unbinned dataset.

## How to use in C++

All the functions are in a [header-only file](widthestimators/src/widthestimators.h). Just include that.

    #include <iostream>
    #include "widthestimators.h"
    
    using widthestimators;
    
    int main()
    {
      TH1F h("h", "h", 100, -5, 5);
      h.FillRandom("gaus");
      std::cout << "s68 : " << binned::s68(h) << std::endl;
      return 0;
    }

## How to use it in python

A proper python package will follow
