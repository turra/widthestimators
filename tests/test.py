import numpy as np
import ROOT

ROOT.gInterpreter.ProcessLine(".L widthestimators/src/widthestimators.cxx+")
s68 = ROOT.widthestimators.binned.s68


class TestBinned(object):
    def test_cumsum(self):
        histo = ROOT.TH1F("h", "h", 10, 0, 10)
        histo.Fill(1)

        values = histo.GetArray()
        values = np.frombuffer(values, np.float32, count=histo.GetNbinsX() + 2)
        cum_sum = np.cumsum(values)

        r = np.array(ROOT.widthestimators.binned.get_cum_sum(histo))

        np.testing.assert_allclose(cum_sum, r)

    def test_s68(self):
        s68 = ROOT.widthestimators.binned.s68
        histo = ROOT.TH1D("h", "h", 1000, -5, 5)
        histo.FillRandom("gaus", 100000)

        w_s68 = s68(histo)
        assert w_s68 > 0
        np.testing.assert_almost_equal(w_s68, 1, decimal=2)
        assert w_s68 < ROOT.widthestimators.binned.s90(histo)

        histo.Reset()
        for v in np.random.normal(0, 0.1, size=100000):
            histo.Fill(v)

        w_s68 = s68(histo)
        np.testing.assert_almost_equal(w_s68, 0.1, decimal=2)
        assert w_s68 < ROOT.widthestimators.binned.s90(histo)

        histo.Reset()
        for v in np.random.normal(1, 0.1, size=100000):
            histo.Fill(v)

        w_s68 = s68(histo)
        np.testing.assert_almost_equal(w_s68, 0.1, decimal=2)
        assert w_s68 < ROOT.widthestimators.binned.s90(histo)

